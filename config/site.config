#!/bin/sh
# Site-specific configurations for Spack
# This file is meant to be sourced in a portable shell script:
# https://www.gnu.org/software/autoconf/manual/autoconf-2.69/html_node/Portable-Shell.html

# The maintainers are expected to create a subdirectory inside the
# directory './sites' containing Spack configuration files
# (e.g. config.yaml, compilers.yaml, packages.yaml, etc.) that will
# extend the common configuration files, which can be found in the
# directory ./common.
#
# If a configuration file needs to be generated at runtime (e.g.
# its contents depend on the current working dir), the executable
# that creates it is expected to reside in the configuration
# subdirectory under the name 'init.config' (see ./common for
# reference). If 'init.config' exists and it is an executable,
# it will be run before applying any site-specific configuration
# file.
#
# By the end of the execution of this script, the variable
# SPACK_SITE_CONFIG must be defined and set to the basename of the
# configuration subdirectory.

# If SPACK_SITE_CONFIG is set but empty, only the common
# configuration will be applied.
SPACK_SITE_CONFIG=

# The maintainers can choose the branch/tag/commit in Spack
# repository that must be installed for their site.
SPACK_VERSION=develop

# The maintainers can decide whether Spack shoud try to detect
# compilers other than those specified in compilers.yaml
SPACK_DETECT_COMPILERS=yes

case $(uname -s) in
  Linux)
    case $(host $(uname -n)) in
      mpipc*.mpimet.mpg.de\ *)
        SPACK_SITE_CONFIG="mpim-jessie"
        SPACK_DETECT_COMPILERS=no ;;
      mlogin*.hpc.dkrz.de\ *)
        SPACK_SITE_CONFIG="dkrz-mistral"
        SPACK_DETECT_COMPILERS=no ;;
      daint*.login.cscs.ch\ *)
        SPACK_SITE_CONFIG="cscs-daint"
        SPACK_DETECT_COMPILERS=no ;;
    esac ;;
  Darwin)
    SPACK_SITE_CONFIG="darwin" ;;
esac    
