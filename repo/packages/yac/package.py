from spack import *


class Yac(AutotoolsPackage):
    """Yet another coupler: coupling ICON component models"""

    homepage = "https://doc.redmine.dkrz.de/YAC/html/"

    version('develop', git='git@git.mpimet.mpg.de:YAC.git', branch='master')
    version('1.3.1', git='git@git.mpimet.mpg.de:YAC.git', tag='1.3.1')

    variant('external-mtime', default=False,
            description='Use external mtime library')

    patch('external_mtime.patch', when='+external-mtime')
    patch('yac_mpirun.patch')

    depends_on('m4', type='build')
    depends_on('autoconf', type='build')
    depends_on('automake', type='build')
    depends_on('libtool', type='build')
    depends_on('libxml2')
    depends_on('lapack')
    depends_on('mpi')

    depends_on('libmtime', when='+external-mtime')

    def configure_args(self):
        # We don't want the configure script to guess which lapack library to
        # use, so we disable the guessing logic as much as we can.
        args = ['--without-lapacke',
                '--without-mkl-lapacke',
                '--without-clapack']

        # We expect that the provider of 'lapack' implements at least the
        # Fortran interface.
        lapack_libs = self.spec['lapack:fortran'].libs

        args.append('LAPACK_FC_LIB=' + lapack_libs.ld_flags)

        return args
