from spack import *
import os
import fnmatch
import inspect


class Libself(AutotoolsPackage):
    """Self is a fortran library that provides maps and vectors for arbitrary
    typed items."""

    homepage = 'https://code.mpimet.mpg.de/projects/self-standard-extendible-library-for-fortran'

    version('develop', git='git@git.mpimet.mpg.de:libself.git',
            branch='master')
    version('icon', git='git@git.mpimet.mpg.de:libself.git',
            branch='libself-icon_derived_variables')

    depends_on('python', type='build')

    patch('install.patch', when='@icon')
    depends_on('autoconf', when='@icon', type='build')

    # Although automake is not used directly, it's required to get recent
    # version of aclocal, which is called by autoreconf.
    depends_on('automake', when='@icon', type='build')

    @when('@icon')
    def patch(self):
        # Old versions of GNU patch seem not to be able to rename files
        for dirpath, _, filenames in os.walk('src'):
            for filename in fnmatch.filter(filenames, '*.F90'):
                new_filename = os.path.splitext(filename)[0] + '.f90'
                os.rename(os.path.join(dirpath, filename),
                          os.path.join(dirpath, new_filename))


    @when('@icon')
    def autoreconf(self, spec, prefix):
        inspect.getmodule(self).autoreconf('-ivf')

