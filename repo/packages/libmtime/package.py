from spack import *


class Libmtime(AutotoolsPackage):
    """Time management library. This project aims to provide time, calendar,
    and event handling for model applications."""

    homepage = 'https://code.mpimet.mpg.de/projects/mtime'

    version('develop', git='git@git.mpimet.mpg.de:libmtime.git',
            branch='develop')

    version('1.0.8', git='git@git.mpimet.mpg.de:libmtime.git', tag='1.0.8')
    version('1.0.7', git='git@git.mpimet.mpg.de:libmtime.git', tag='1.0.7')
    version('1.0.6', git='git@git.mpimet.mpg.de:libmtime.git', tag='1.0.6')

    variant('examples', default=True, description='Enable examples')

    depends_on('autoconf', type='build')
    depends_on('automake', type='build')
    depends_on('libtool', type='build')

    patch('simulate_iau.patch', when='@1.0.7')
    patch('example_option_develop.patch', when='@develop')
    patch('example_option_master.patch', when='@:1.0.8')

    force_autoreconf = True

    parallel = False

    def configure_args(self):
        return self.enable_or_disable('examples')
